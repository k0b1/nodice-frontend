import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";

//import { HeaderSearchComponent } from "../../header/header-search/header-search.component";
import { GetCategoriesService } from "../services/get-from-database/get-categories.service";
import { environment } from "./../../environments/environment";

@Component({
  selector: "app-categories",
  templateUrl: "./categories.component.html",
  styleUrls: ["./categories.component.scss"]
})
export class CategoriesComponent implements OnInit {
  hoveringIcon: string = "";

  displayData: string = "none";

  currentCategories: any[];
  urlBase: string = environment.urlBase;
  
  @ViewChild("elem", { static: false }) findChild: ElementRef;

  constructor(private getCategories: GetCategoriesService) {}

  ngOnInit() {
    this.getCategoriesFromDatabase();
  }

  onCategoryMouseEnter(event) {
    
    this.displayData = "block";
    let target = event.target || event.srcElement || event.currentTarget;
    let idAttr = target.attributes.id;
    idAttr.nodeValue = "showBackground";
    let findChild = target.childNodes;
    findChild[0].className = "hidden-title";
      findChild[1].className = "number-of-profiles";

  }
  onCategoryMouseLeave(event) {
    this.displayData = "none";
    let target = event.target || event.srcElement || event.currentTarget;
    let idAttr = target.attributes.id;
    idAttr.nodeValue = "hideBackground";
    let findChild = target.childNodes;
    findChild[0].className = "d-none";
    findChild[1].className = "d-none";
  }

  getCategoriesFromDatabase() {
      this.getCategories.getCategoriesFromDatabase().subscribe(response => {
                console.log("target", response)

      this.currentCategories = response;
    });
  }
}
