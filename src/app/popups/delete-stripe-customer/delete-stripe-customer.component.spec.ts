import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteStripeCustomerComponent } from './delete-stripe-customer.component';

describe('DeleteStripeCustomerComponent', () => {
  let component: DeleteStripeCustomerComponent;
  let fixture: ComponentFixture<DeleteStripeCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteStripeCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteStripeCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
