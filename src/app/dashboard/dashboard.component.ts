import { Component, OnInit } from "@angular/core";
import { AuthenticateService } from "../services/authenticate/authenticate.service";
import { ActivatedRoute, Router, NavigationEnd } from "@angular/router";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {
  userType: string;
  userID: string;
  constructor(
    private auth: AuthenticateService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.auth
      .authenticateUser(
        this.auth.authenticateUserObject.authenticateNonce,
        this.auth.authenticateUserObject.authenticateUser
      )
      .subscribe(response => {
        this.userType = response.role;
        this.userID = response.ID;
      });

    // this.route.params.subscribe(params => {
    //   console.log(params);
    // });
  }
}
