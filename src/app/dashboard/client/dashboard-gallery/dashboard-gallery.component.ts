import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject
} from "@angular/core";
import { AuthenticateService } from "src/app/services/authenticate/authenticate.service";
import { ActivatedRoute } from "@angular/router";

// import { DashboardNavigationClientComponent } from '../dashboard-navigation-client/dashboard-navigation-client.component';
import { BlockOtherUsersDashboardService } from "src/app/services/block-other-users-dashboard.service";
// import { HeaderDashboardComponent } from '../../../../header/header-dashboard/header-dashboard.component';
import { UploadImagesService } from "src/app/services/write-to-database/upload-images.service";
// import { HttpEventType, HttpResponse } from '@angular/common/http';
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import { GetImageOrderService } from "src/app/services/get-from-database/get-image-order.service";

import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { DeleteImageComponent } from "../../../popups/delete-image/delete-image.component";

import { environment } from "../../../../environments/environment";

@Component({
  selector: "app-dashboard-gallery",
  templateUrl: "./dashboard-gallery.component.html",
  styleUrls: ["./dashboard-gallery.component.scss"]
})
export class DashboardGalleryComponent implements OnInit {
  // baseUrl: string = "http://localhost/svetRada/nodice/wp-json/wp/v2";
  // baseUrl: string = "http://nodice.gnf.dk/wp-json/wp/v2";
  baseUrl: string = environment.apiHost;

  urlUploadImage: string = this.baseUrl + "/media";

  @ViewChild("upload", { static: false }) upload: ElementRef;
  savedNonce: string = null;
  loggedIn: boolean = false;
  isLoggedInLocalStorage: string;

  userIDRoute: string;
  notYourPage: boolean = false; //if someone change URL to see other clients dashboard page

  defaultPage = false;
  imagesArray: any = [];

  showMetaArray: boolean; // display new order of items
  imagesData = {
    src: ""
  };
  constructor(
    private authService: AuthenticateService,
    private route: ActivatedRoute,
    private blockService: BlockOtherUsersDashboardService,
    private workWithImage: UploadImagesService,
    private getImageOrder: GetImageOrderService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    if (!this.authService.authenticateUserObject.authenticateNonce) {
      this.savedNonce = localStorage.getItem("user");
    } else {
      this.savedNonce = this.authService.authenticateUserObject.authenticateNonce;
    }

    this.isLoggedInLocalStorage = localStorage.getItem("isUserLoggedIn");

    let result = this.authService.checkIfUserIsLoggedIn(
      this.authService.authenticateUserObject.authenticateNonce,
      this.savedNonce,
      this.isLoggedInLocalStorage
    );

    this.userIDRoute = this.route.snapshot.params["clientID"];
    /** check allowed pages */
    this.blockService.blockOtherUsersDashboard(this.userIDRoute);

    this.getImages();
    // this.getSavedImageOrder()
    // if ( this.showMetaArray ) {
    //   this.getSavedImageOrder();
    // } else {
    //   this.getImages();
    // }
  }

  //default order as images are uploaded
  getImages() {
    this.savedNonce = localStorage.getItem("user");
    this.isLoggedInLocalStorage = localStorage.getItem("isUserLoggedIn");

    this.authService
      .authenticateUser(this.savedNonce, this.isLoggedInLocalStorage)
      .subscribe(response => {
        this.workWithImage.getClientImages(response.ID).subscribe(response => {
          if (response.length > 0) {
            //there are images uploaded by the client, so list them
            this.defaultPage = false;

            // this.imagesArray = response;

            // image order after drag and drop, saved in users meta table
            this.getImageOrder
              .getImageOrder(this.savedNonce)
              .subscribe(response => {
                if (response) {
                  //there are images uploaded by the client, so list them
                  this.defaultPage = false;

                  this.imagesArray = response;
                  this.showMetaArray = true;
                } else {
                  /** no images uploaded by client, show default page */

                  this.defaultPage = true;
                  this.showMetaArray = false;
                }
              });
          } else {
            /** no images uploaded by client, show default page */

            this.defaultPage = true;
          }
        });
      });
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.imagesArray, event.previousIndex, event.currentIndex);

    //save new order in user meta tbale
    this.workWithImage
      .uploadImageOrder(this.imagesArray, this.savedNonce)
      .subscribe(response => {
        console.log("php", response);
        //        this.imagesArray = response;
      });
  }

  selectFile(event) {
    this.uploadFile(event.target.files);
  }

  uploadFile(files: FileList) {
    if (files.length == 0) {
      return;
    }
    let file: File = files[0];
    this.workWithImage
      .uploadImage(this.urlUploadImage, file)
      .subscribe(response => {
        this.imagesArray.push(response);
        this.defaultPage = false;
        //add new image upload to user meta table,
        //save new order in user meta tbale
        this.workWithImage
          .uploadImageOrder(this.imagesArray, this.savedNonce)
          .subscribe(response => {
            // this.imagesArray = response;
          });
      });
  }

  onDeleteClick(event) {
    let parentNode = event.path[3];
    let imageID = parentNode.childNodes[0].id;

    const dialogRef = this.dialog.open(DeleteImageComponent, {
      width: "500px",
      height: "240px"
      //  data: {name: "helo", animal: "world"}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        for (let image in this.imagesArray) {
          if (this.imagesArray[image].id == imageID) {
            this.imagesArray.splice(Number(image), 1);
          }
        }
        this.workWithImage
          .deleteImagesOnClick(imageID, this.savedNonce)
          .subscribe(response => {
            /** update new image order */

            this.workWithImage
              .uploadImageOrder(this.imagesArray, this.savedNonce)
              .subscribe(response => {});
          });
      }
    });
  }
}
