import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { AuthenticateService } from 'src/app/services/authenticate/authenticate.service';
import { ActivatedRoute } from '@angular/router';
import { BlockOtherUsersDashboardService } from 'src/app/services/block-other-users-dashboard.service';

import { HeaderDashboardComponent } from '../../../shared/layout/header/header-dashboard/header-dashboard.component';
import { StripeService } from 'src/app/services/stripe/stripe.service';
@Component({
  selector: 'app-dashboard-plan',
  templateUrl: './dashboard-plan.component.html',
  styleUrls: ['./dashboard-plan.component.scss']
})
export class DashboardPlanComponent implements OnInit {

  savedNonce: string = null;
  loggedIn : boolean = false;
  isLoggedInLocalStorage: string;

  userIDRoute: string;
  notYourPage: boolean = false; //if someone change URL to see other clients dashboard page

  activePlan: any;

  @ViewChild('free', {static: false}) free: ElementRef;
  constructor( 
    private authService: AuthenticateService,
    private route: ActivatedRoute,
    private blockService: BlockOtherUsersDashboardService,
    private stripe: StripeService
  ) { }

  ngOnInit() {
    this.savedNonce = localStorage.getItem('user');
    this.isLoggedInLocalStorage = localStorage.getItem('isUserLoggedIn');
    
    /** check login status */
    this.authService.checkIfUserIsLoggedIn( this.authService.authenticateUserObject.authenticateNonce, this.savedNonce,this.isLoggedInLocalStorage );

    this.userIDRoute = this.route.snapshot.params['clientID'];

    /** check allowed pages */
    this.blockService.blockOtherUsersDashboard( this.userIDRoute );
    
    this.getCurrentPlan();

  }

  onPlanClick( elem: HTMLElement ) {
    
    
    let chosenPlan;
    switch( elem.className ) {
      case "free plan-box":
        chosenPlan = "free";
        this.activePlan = "free";
        break;
      case "popular plan-box":
        chosenPlan = "popular";
        this.activePlan = "popular";
        break;
      case "social-butterfly plan-box":
        chosenPlan = "social-butterfly";
        this.activePlan = "social-butterfly";
        break;
      case "influencer plan-box":
        chosenPlan = "influencer";
        this.activePlan = "influencer";
        break;
    }
            
    this.stripe.saveStripePlan( this.savedNonce, chosenPlan ).subscribe( response => {
      
    })
  }

  getCurrentPlan() {
    this.stripe.getStripePlan( this.savedNonce ).subscribe( response => {
     
      this.activePlan = response;
    })
  }

}
