import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute, Router, Params, NavigationEnd } from "@angular/router";

@Component({
  selector: "app-dashboard-navigation-client",
  templateUrl: "./dashboard-navigation-client.component.html",
  styleUrls: ["./dashboard-navigation-client.component.scss"]
})
export class DashboardNavigationClientComponent implements OnInit {
  // activeNavLink : string = "active-nav-link";
  @Input() userID: string;
  currentPage;
  isGeneralActive: string = "";
  isGalleryActive: string = "";
  isPlanActive: string = "";
  isBillingActive: string = "";
  navigationLinks: { name: string; active: string }[] = [
    {
      name: "general",
      active: "isGeneralActive"
    },
    {
      name: "gallery",
      active: "isGalleryActive"
    },
    {
      name: "plan",
      active: "isPlanActive"
    },
    {
      name: "billing",
      active: "isBillingActive"
    }
  ];
  userIDRoute: string;

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.userIDRoute = this.router.url.split("/")[2];
    this.currentPage = this.router.url.split("/")[3];
    if (this.currentPage == "general") {
      this.isGeneralActive = "active-nav-link";
    } else if (this.currentPage == "gallery") {
      this.isGalleryActive = "active-nav-link";
    } else if (this.currentPage == "plan") {
      this.isPlanActive = "active-nav-link";
    } else if (this.currentPage == "billing") {
      this.isBillingActive = "active-nav-link";
    }

    this.router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        if (val.url) this.currentPage = val.url.split("/")[3];

        if (this.currentPage == "general") {
          this.isActiveLink("general");
        } else if (this.currentPage == "gallery") {
          this.isActiveLink("gallery");
        } else if (this.currentPage == "plan") {
          this.isActiveLink("plan");
        } else if (this.currentPage == "billing") {
          this.isActiveLink("billing");
        }
      }
    });
  }

  isActiveLink(link) {
    let active = "";

    this.navigationLinks.filter(item => {
      if (item.name === link) {
        console.log(item);
        console.log(this[item.active]);
        this[item.active] = "active-nav-link";
        return active;
      } else {
        this[item.active] = "";
      }
    });
  }
}
