import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardNavigationClientComponent } from './dashboard-navigation-client.component';

describe('DashboardNavigationClientComponent', () => {
  let component: DashboardNavigationClientComponent;
  let fixture: ComponentFixture<DashboardNavigationClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardNavigationClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardNavigationClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
