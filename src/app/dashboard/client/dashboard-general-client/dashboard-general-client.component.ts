import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { AuthenticateService } from "src/app/services/authenticate/authenticate.service";
import { ActivatedRoute } from "@angular/router";

import { DashboardNavigationClientComponent } from "../dashboard-navigation-client/dashboard-navigation-client.component";
import { ManipulateTextService } from "src/app/services/write-to-database/manipulate-client-text.service";
import { BlockOtherUsersDashboardService } from "src/app/services/block-other-users-dashboard.service";

import { HeaderDashboardComponent } from "../../../shared/layout/header/header-dashboard/header-dashboard.component";

import { SaveClientContactDataService } from "src/app/services/write-to-database/save-client-contact-data.service";
import { NgForm } from "@angular/forms";
import { GetClientDataService } from "src/app/services/get-from-database/get-client-data.service";
import { GetServicesService } from "src/app/services/get-from-database/get-services.service";
import { SaveServicesService } from "src/app/services/write-to-database/save-services.service";
import { DisplayServicesComponent } from "../../../display-services/display-services.component";

@Component({
  selector: "app-dashboard-general",
  templateUrl: "./dashboard-general-client.component.html",
  styleUrls: ["./dashboard-general-client.component.scss"]
})
export class DashboardGeneralClientComponent implements OnInit {
  userID: number;
  notYourPage: boolean = false; //if someone change URL to see other clients dashboard page

  contentEditable: boolean = false;
  isEditText: string = "";

  savedNonce: string = null;
  loggedIn: boolean = false;
  isLoggedInLocalStorage: string;

  clientProfilePageName: string;
  clientProfileText: string;

  phone: string;
  email: string;
  website: string;

  displayServices: any; //all nodice services
  displayChosenServices: any = [];
  displayServicesBox: boolean = false;
  pageID: string;

  userIDRoute: string;
  @ViewChild("textarea", { static: false }) textarea: ElementRef;

  constructor(
    private authenticateService: AuthenticateService,
    private route: ActivatedRoute,
    private pageText: ManipulateTextService,
    private blockService: BlockOtherUsersDashboardService,
    private saveContactData: SaveClientContactDataService,
    private getContactData: GetClientDataService,
    private getServices: GetServicesService,
    private saveServices: SaveServicesService
  ) {}

  ngOnInit() {
    this.savedNonce = localStorage.getItem("user");
    this.isLoggedInLocalStorage = localStorage.getItem("isUserLoggedIn");

    this.authenticateService.checkIfUserIsLoggedIn(
      this.authenticateService.authenticateUserObject.authenticateNonce,
      this.savedNonce,
      this.isLoggedInLocalStorage
    );

    this.authenticateService
      .authenticateUser(this.savedNonce, this.isLoggedInLocalStorage)
      .subscribe(response => {
        if (response.role == "customer") {
          this.notYourPage = true;
        } else {
          /** get text */
          this.getClientProfileText();
        }
      });
    this.userIDRoute = this.route.snapshot.params["clientID"];

    /** check allowed pages */
    this.blockService.blockOtherUsersDashboard(this.userIDRoute);

    /** get user contact data */
    this.getClientContactData();
  }

  editTextarea() {
    this.contentEditable = true;
    this.textarea.nativeElement.focus();
    this.isEditText = "edit-general-text";
  }

  onCancel() {
    this.contentEditable = false;

    this.textarea.nativeElement.blur();
    this.isEditText = "";
  }

  /** get client profile text */
  getClientProfileText() {
    this.pageText.getClientText(this.userIDRoute).subscribe(response => {
      if (this.authenticateService.userData) {
        // to prevent error in console

        if (this.userIDRoute == this.authenticateService.userData.ID) {
          if (this.authenticateService.userData.ID == response[0].author) {
            if (response[0].status === "publish") {
              this.clientProfilePageName = response[0].title.rendered;
              this.clientProfileText = response[0].content.rendered;

              this.pageID = response[0].id; //to be used for saving of services

              /** set data to be send when updating page */
              this.pageText.userObject.postID = response[0].id;
              this.pageText.userObject.postAuthor = response[0].author;
              this.pageText.userObject.postTitle = response[0].title.rendered;
              this.pageText.userObject.postExcerpt = response[0].excerpt;
              this.pageText.userObject.postStatus = response[0].status;
              this.pageText.userObject.postCategories = response[0].categories;

              /** display all services in select box */
              this.displayServicesInSelectBox(this.pageID);
            }
          }
        } else {
          this.notYourPage = true;
          console.log("it is a wrong 'em boyo");
        }
      }
    });
  }

  /** on submit, save Client text to Wordpress database */
  submitClientProfileText() {
    if (!this.authenticateService.authenticateUserObject.authenticateNonce) {
      this.savedNonce = localStorage.getItem("user");
    } else {
      this.savedNonce = this.authenticateService.authenticateUserObject.authenticateNonce;
    }

    this.pageText.userObject.postContent = this.textarea.nativeElement.innerText; //new page content text value

    /** save text to WP database.
     * @param this.pageText.userObject defined in manipulate-client-service.ts, basic user data
z     * @param savedNonce, wp nonce, saved in local storage, or in current nodice app state
     */
    this.pageText
      .saveClientText(this.pageText.userObject, this.savedNonce)
      .subscribe(response => {
        if (response) {
          //everything ok
        } else {
          //response is zero, there was some problem
        }
      });
  }

  saveClientContactData(formData: NgForm) {
    console.log("get contact", formData);
    let userData = {
      phone: formData.value.phone,
      email: formData.value.email,
      website: formData.value.website
    };
    this.saveContactData
      .saveUserData(this.savedNonce, userData)
      .subscribe(response => {
        console.log("save contact", response);
      });
  }

  getClientContactData() {
    this.getContactData.getClientData(this.savedNonce).subscribe(response => {
      console.log("get contact", response);
      this.phone = response["phone"];
      this.email = response["email"];
      this.website = response["website"];
    });
  }

  addServices() {
    this.displayServicesBox = true;
  }
  displayServicesInSelectBox(pageID) {
    this.getServices.getServicesFromDatabase().subscribe(response => {
      this.displayServices = response;

      /** display nodice services data for current page */
      this.getServices.displayChosenServices(pageID).subscribe(response => {
        for (let service in this.displayServices) {
          for (let serviceID in response["services"]) {
            if (
              this.displayServices[service].id ==
              response["services"][serviceID]
            ) {
              this.displayChosenServices.push(
                this.displayServices[service].name
              );
            }
          }
        }
      });
    });
  }

  saveServicesOnSubmit(form: NgForm) {
    this.saveServices
      .saveServicesForPage(this.pageID, form.value)
      .subscribe(response => {
        console.log(response);
        this.displayServicesBox = false;
        this.displayChosenServices = response;
      });
  }
}
