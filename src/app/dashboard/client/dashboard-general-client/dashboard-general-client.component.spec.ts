import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardGeneralClientComponent } from './dashboard-general-client.component';

describe('DashboardGeneralComponent', () => {
  let component: DashboardGeneralClientComponent;
  let fixture: ComponentFixture<DashboardGeneralClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardGeneralClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardGeneralClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
