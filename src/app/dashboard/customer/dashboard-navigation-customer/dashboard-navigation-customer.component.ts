import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthenticateService } from "src/app/services/authenticate/authenticate.service";

@Component({
  selector: "app-dashboard-navigation-customer",
  templateUrl: "./dashboard-navigation-customer.component.html",
  styleUrls: ["./dashboard-navigation-customer.component.scss"]
})
export class DashboardNavigationCustomerComponent implements OnInit {
  currentPage: string;
  isGeneralActive: string = "";
  isReviewsActive: string = "";

  userIDRoute: string;

  constructor(private router: Router) {}

  ngOnInit() {
    this.userIDRoute = this.router.url.split("/")[2];
    this.currentPage = this.router.url.split("/")[3];

    if (this.currentPage == "general") {
      this.isGeneralActive = "active-nav-link";
    } else if (this.currentPage == "reviews") {
      this.isReviewsActive = "active-nav-link";
    }
  }
}
