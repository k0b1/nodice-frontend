import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Output
} from "@angular/core";
import { AuthenticateService } from "src/app/services/authenticate/authenticate.service";
import { ActivatedRoute, UrlSegment } from "@angular/router";
import { ManipulateTextService } from "src/app/services/write-to-database/manipulate-client-text.service";
import { BlockOtherUsersDashboardService } from "src/app/services/block-other-users-dashboard.service";

import { NgForm } from "@angular/forms";
import { UpdateCustomerDataService } from "src/app/services/write-to-database/update-customer-data.service";
import { Observable } from "rxjs";
import { EventEmitter } from "events";

@Component({
  selector: "app-dashboard-general",
  templateUrl: "./dashboard-general.component.html",
  styleUrls: ["./dashboard-general.component.scss"]
})
export class DashboardGeneralComponent implements OnInit {
  userID: number;
  activeSubPage: Observable<UrlSegment[]>;
  savedNonce: string = null;
  loggedIn: boolean = false;
  isLoggedInLocalStorage: string;

  @Output() isActiveSubpage = new EventEmitter();

  userIDRoute: string;
  userData: string;

  constructor(
    private authenticateService: AuthenticateService,
    private route: ActivatedRoute,
    private pageText: ManipulateTextService,
    private blockService: BlockOtherUsersDashboardService,
    private updateCustomer: UpdateCustomerDataService
  ) {}

  ngOnInit() {
    this.userIDRoute = this.route.snapshot.params["customerID"];
    /** get text */
    /** check allowed pages */
    this.blockService.blockOtherUsersDashboard(this.userIDRoute);

    this.activeSubPage = this.route.url["value"];
    if (this.activeSubPage[1] == "general") {
      //      this.isGeneralActive = "active-nav-link";
      this.isActiveSubpage.emit("general");
    } else if (this.activeSubPage[2].path == "reviews") {
      //      this.isReviewsActive = "active-nav-link";
    }
  }

  onSubmitUserData(form: NgForm) {
    let userData: {};
    this.authenticateService
      .authenticateUser(
        this.authenticateService.authenticateUserObject.authenticateNonce,
        this.authenticateService.authenticateUserObject.authenticateUser
      )
      .subscribe(response => {
        this.userData = response;

        userData = {
          ID: response.ID,
          display_name: form.value["customer-username"]
        };
        this.updateCustomer.updateUserData(userData).subscribe(response => {});
      });
  }

  onSubmitPassword(form: NgForm) {
    let userData: {};
    // let wpnonce = localStorage.getItem("user");

    //there two inputs are the same, let's go further
    userData = {
      customer_password: form.value["customer-password"],
      customer_confirm_password: form.value["customer-confirm-password"]
    };
    this.updateCustomer
      .updatePassword(
        userData,
        this.authenticateService.authenticateUserObject.authenticateNonce
      )
      .subscribe((response: { nonce; status }) => {
        // new nonce and cookie are created. update nonce for whole app
        // this.authenticateService.authenticateUserObject.authenticateNonce =
        //   response.nonce;
        // this.authenticateService.isLoggedIn = true;

        // localStorage.setItem("user", response.nonce);
        // localStorage.setItem("isUserLoggedIn", "true");

        if (response.status === "password_changed") {
          alert("Password successfully changed! ");
        }
      });
  }
}
