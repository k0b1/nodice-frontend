import { Component, OnInit } from "@angular/core";
import { AuthenticateService } from "src/app/services/authenticate/authenticate.service";
import { ActivatedRoute } from "@angular/router";
import { ManipulateTextService } from "src/app/services/write-to-database/manipulate-client-text.service";
import { BlockOtherUsersDashboardService } from "src/app/services/block-other-users-dashboard.service";
import { GetCommentsService } from "src/app/services/get-from-database/get-comments.service";

@Component({
  selector: "app-dashboard-reviews",
  templateUrl: "./dashboard-reviews.component.html",
  styleUrls: ["./dashboard-reviews.component.scss"]
})
export class DashboardReviewsComponent implements OnInit {
  userID: number;
  userIDRoute: string;
  listComments: {};

  constructor(
    private authenticateService: AuthenticateService,
    private route: ActivatedRoute,
    private pageText: ManipulateTextService,
    private blockService: BlockOtherUsersDashboardService,
    private getCommentsData: GetCommentsService
  ) {}

  ngOnInit() {
    this.userIDRoute = this.route.snapshot.params["customerID"];
    /** check allowed pages */
    this.blockService.blockOtherUsersDashboard(this.userIDRoute);

    /** get comments for current user */
    this.getCommentsFromDatabase();
  }

  getCommentsFromDatabase() {
    this.getCommentsData
      .getCommentsByUser(
        this.userIDRoute,
        this.authenticateService.authenticateUserObject.authenticateNonce
      )
      .subscribe(response => {
        this.listComments = response;

        this.getCommentsData.getCommentsRatings(this.listComments);

        for (let comment in this.listComments) {
          this.getCommentsData
            .getPageTitleOfComments(this.listComments[comment].comment_post_ID)
            .subscribe(response => {
              this.listComments[comment]["pageTitle"] = response;
            });
        }
      });
  }
}
