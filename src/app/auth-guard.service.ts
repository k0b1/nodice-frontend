import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthenticateService } from "./services/authenticate/authenticate.service";
import { Injectable } from "@angular/core";

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthenticateService, private router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    let checkNonce = this.auth.authenticateUserObject.authenticateNonce;
    console.log("authGuard", checkNonce);
    console.log("1", this.auth.authenticateUserObject.authenticateUser);
    if (checkNonce) {
      return true;
    } else {
      this.router.navigate(["/login"]);
      return false;
    }
  }
}
