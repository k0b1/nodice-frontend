import { Component, OnInit } from "@angular/core";
import { AuthenticateService } from "src/app/services/authenticate/authenticate.service";
import { Router, ActivatedRoute } from "@angular/router";
import { LogoutService } from "src/app/services/logout.service";
import { SidebarComponent } from "../sidebar/sidebar.component";

@Component({
  selector: "app-header-dashboard",
  templateUrl: "./header-dashboard.component.html",
  styleUrls: ["./header-dashboard.component.scss"]
})
export class HeaderDashboardComponent implements OnInit {
  userID: string;
  userType: string;

  /** define header font colors on different pages */
  fontColor: string = "frontpage-font-color";

  nonceInLocalStorage: string = null;
  loggedIn: boolean = false;
  isLoggedInLocalStorage: string;

  displaySidebarStyle: string = "none";

  constructor(
    private isUserLoggedIn: AuthenticateService,
    private router: Router,
    private route: ActivatedRoute,
    private logout: LogoutService
  ) {}

  ngOnInit() {
    this.nonceInLocalStorage = localStorage.getItem("user");
    this.isLoggedInLocalStorage = localStorage.getItem("isUserLoggedIn");

    this.isUserLoggedIn.checkIfUserIsLoggedIn(
      this.isUserLoggedIn.authenticateUserObject.authenticateNonce,
      this.nonceInLocalStorage,
      this.isLoggedInLocalStorage
    );

    if (
      this.isUserLoggedIn.authenticateUser(
        this.nonceInLocalStorage,
        this.isLoggedInLocalStorage
      )
    ) {
      this.isUserLoggedIn
        .authenticateUser(this.nonceInLocalStorage, this.isLoggedInLocalStorage)
        .subscribe(
          response => {
            if (response.authenticate === "false") {
              //logout
              this.isLoggedInLocalStorage = "false";

              localStorage.setItem("user", "");
              localStorage.setItem("isUserLoggedIn", "false");
              this.router.navigate(["/login"]);
            } else {
              this.userID = response.ID;
              this.userType = response.role;
            }
          },
          error => {
            if (error.statusText == "Forbidden") {
//              
              localStorage.setItem("user", "");
              localStorage.setItem("isUserLoggedIn", "false");
              this.router.navigate(["/login"]);
            }
          }
        );
    } else {
    }
  }

  showSidebar() {
    this.displaySidebarStyle = "block";
  }

  onHideSidebar(event: string) {
    if (event == "none") {
      this.displaySidebarStyle = "none";
    }
  }

  onLogout() {
    /** two varaible definitions below added to refresh variables - if user change password, bug occuer with wpnonce */
    this.nonceInLocalStorage = localStorage.getItem("user");
    this.isLoggedInLocalStorage = localStorage.getItem("isUserLoggedIn");

    this.logout.onLogout(
      this.isLoggedInLocalStorage,
      this.isUserLoggedIn.authenticateUserObject.authenticateNonce,
      this.nonceInLocalStorage
    );
  }
}
