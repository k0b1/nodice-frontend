import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthenticateService } from 'src/app/services/authenticate/authenticate.service';
import { LogoutService } from 'src/app/services/logout.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() showSidebar: string;
    @Input() userID;
    @Input() userType;
  @Output() hideSidebar = new EventEmitter<string>();

  displaySidebarStyle: string = "none";
  

  nonceInLocalStorage: string = null;
  isLoggedInLocalStorage: string;

  constructor(
    private isUserLoggedIn: AuthenticateService, 
    private router: Router,
    private route: ActivatedRoute,
    private logout: LogoutService ) { }

  ngOnInit() {
    this.nonceInLocalStorage = localStorage.getItem('user');
    this.isLoggedInLocalStorage = localStorage.getItem('isUserLoggedIn');
  }
  
  closeSidebar() {
    this.hideSidebar.emit("none");
  }
  onLogout(){
    this.logout.onLogout(
      this.isLoggedInLocalStorage, 
      this.isUserLoggedIn.authenticateUserObject.authenticateNonce, 
      this.nonceInLocalStorage );
 
  }
}
