import { Component, OnInit } from "@angular/core";
import { AuthenticateService } from "src/app/services/authenticate/authenticate.service";
import { Router, ActivatedRoute } from "@angular/router";
import { LogoutService } from "src/app/services/logout.service";
import { NgForm, FormControl, FormGroup, Validators } from "@angular/forms";
import { Observable } from "rxjs";
import { startWith, map } from "rxjs/operators";
import { GetServicesService } from "src/app/services/get-from-database/get-services.service";
import { SearchNodiceService } from "src/app/services/get-from-database/search-nodice.service";
import { GetCategoriesService } from "src/app/services/get-from-database/get-categories.service";

@Component({
  selector: "app-header-search",
  templateUrl: "./header-search.component.html",
  styleUrls: ["./header-search.component.scss"]
})
export class HeaderSearchComponent implements OnInit {
  userID: string;
  userType: string;

  myControl = new FormControl();
  options: string[] = ["One", "Two", "Three"];
  filteredOptions: Observable<string[]>;

  stateSelect = null;

  servicesArray: any[] = [];

  /** define header font colors on different pages */
  fontColor: string = "frontpage-font-color";

  nonceInLocalStorage: string;
  loggedIn: boolean = false;
  isLoggedInLocalStorage: string;

  displaySidebarStyle: string = "none";

  /** default Select values */
  categories: string = "Categories";

  allServices;
  allCategories: any[] = [];

  constructor(
    private isUserLoggedIn: AuthenticateService,
    private router: Router,
    private route: ActivatedRoute,
    private logout: LogoutService,
    private getServices: GetServicesService,
    private search: SearchNodiceService,
    private getCategories: GetCategoriesService
  ) {}

  ngOnInit() {
    this.nonceInLocalStorage = localStorage.getItem("user");
    this.isLoggedInLocalStorage = localStorage.getItem("isUserLoggedIn");

    this.isUserLoggedIn.checkIfUserIsLoggedIn(
      this.isUserLoggedIn.authenticateUserObject.authenticateNonce,
      this.nonceInLocalStorage,
      this.isLoggedInLocalStorage
    );

    if (
      this.isUserLoggedIn.authenticateUser(
        this.nonceInLocalStorage,
        this.isLoggedInLocalStorage
      )
    ) {
      this.isUserLoggedIn
        .authenticateUser(this.nonceInLocalStorage, this.isLoggedInLocalStorage)
        .subscribe(
          response => {
            this.userID = response.ID;
            this.userType = response.role;
          },
          error => {
            if ((error.statusTest = "Forbidden")) {
              localStorage.setItem("user", "");
              localStorage.setItem("isUserLoggedIn", "false");
              // this.router.navigate(['/login']);
            }
          }
        );
    }

    this.showServices();
    this.showCategories();
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    this.getServices.getServicesFromDatabase().subscribe(response => {});
    return this.servicesArray.filter(option =>
      option.toLowerCase().includes(filterValue)
    );
  }

  showServices() {
    this.getServices.getServicesFromDatabase().subscribe(response => {
      /** get service names from database */
      this.allServices = response;
      for (let service in response) {
        this.servicesArray.push(response[service].name);
      }
      /** filter options for Nodice services */
      console.log("services", response);
      this.filteredOptions = this.myControl.valueChanges.pipe(
        startWith(""),
        map(value => this._filter(value))
      );
    });
  }

  showCategories() {
    this.getCategories.getCategoriesFromDatabase().subscribe(response => {
      this.allCategories = response;
    });
  }

  showSidebar() {
    this.displaySidebarStyle = "block";
  }

  onHideSidebar(event: string) {
    if (event == "none") {
      this.displaySidebarStyle = "none";
    }
  }

  onLogout() {
    this.logout.onLogout(
      this.isLoggedInLocalStorage,
      this.isUserLoggedIn.authenticateUserObject.authenticateNonce,
      this.nonceInLocalStorage
    );
  }

  searchNodice(form: NgForm) {
    //    console.log("all services", this.allCategories);

    let serviceID: string = this.myControl.value;
    let categoryID: string = "";
    //    let ratingsValue: string = "";
    let rating: string = form.value["search-ratings"];

    for (let service in this.allServices) {
      if (this.allServices[service].name == this.myControl.value) {
        serviceID = this.allServices[service].id;
      }
    }

    /** selected category */
    for (let category in this.allCategories) {
      if (this.allCategories[category].id == form.value["select-categories"]) {
        categoryID = this.allCategories[category].id;
      }
    }

    /** take input value, compare it to the service/category/ratings values in WP database and offer selection while writing. After chosing approving selection use  getPagesForCategory function from get-categories service */

    this.router.navigate(["/search/"], {
      queryParams: {
        services: serviceID || 0,
        category: categoryID || 0,
        ratings: rating || 0
      }
    });
  }
}
