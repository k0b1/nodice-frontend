import { Component, OnInit } from "@angular/core";
import { AuthenticateService } from "../../../services/authenticate/authenticate.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { LogoutService } from "../../../services/logout.service";
import { SidebarComponent } from "../header/sidebar/sidebar.component";

@Component({
  selector: "app-header-home",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderHomeComponent implements OnInit {
  userID: string;
  userType: string;
  /** define header font colors on different pages */
  fontColor: string = "frontpage-font-color";

  nonceInLocalStorage: string = null;
  loggedIn: boolean = false;
  isLoggedInLocalStorage: string;

  displaySidebarStyle: string = "none";

  constructor(
    private isUserLoggedIn: AuthenticateService,
    private router: Router,
    private route: ActivatedRoute,
    private logout: LogoutService
  ) {}

  ngOnInit() {
    this.nonceInLocalStorage = localStorage.getItem("user");
    this.isLoggedInLocalStorage = localStorage.getItem("isUserLoggedIn");

    this.isUserLoggedIn.checkIfUserIsLoggedIn(
      this.isUserLoggedIn.authenticateUserObject.authenticateNonce,
      this.nonceInLocalStorage,
      this.isLoggedInLocalStorage
    );

    if (
      this.isUserLoggedIn.authenticateUser(
        this.nonceInLocalStorage,
        this.isLoggedInLocalStorage
      )
    ) {
      this.isUserLoggedIn
        .authenticateUser(this.nonceInLocalStorage, this.isLoggedInLocalStorage)
        .subscribe(
          response => {
            this.userID = response.ID;
            this.userType = response.role;
          },
          error => {
            if ((error.statusTest = "Forbidden")) {
              localStorage.setItem("user", "");
              localStorage.setItem("isUserLoggedIn", "false");
              // this.router.navigate(['/login']);
            }
          }
        );
    } else {
      localStorage.setItem("user", "");
      localStorage.setItem("isUserLoggedIn", "false");
    }
  }

  showSidebar() {
    this.displaySidebarStyle = "block";
  }

  onHideSidebar(event: string) {
    if (event == "none") {
      this.displaySidebarStyle = "none";
    }
  }

  onLogout() {
    this.logout.onLogout(
      this.isLoggedInLocalStorage,
      this.isUserLoggedIn.authenticateUserObject.authenticateNonce,
      this.nonceInLocalStorage
    );

  }
}
