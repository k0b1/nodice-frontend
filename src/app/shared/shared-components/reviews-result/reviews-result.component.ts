import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-reviews-result',
  templateUrl: './reviews-result.component.html',
  styleUrls: ['./reviews-result.component.scss']
})
export class ReviewsResultComponent implements OnInit {
    @Input() result: {};
  constructor() { }

    ngOnInit() {
        console.log('review result', this.result)
  }

}
