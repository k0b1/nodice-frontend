import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewsResultComponent } from './reviews-result.component';

describe('ReviewsResultComponent', () => {
  let component: ReviewsResultComponent;
  let fixture: ComponentFixture<ReviewsResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewsResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewsResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
