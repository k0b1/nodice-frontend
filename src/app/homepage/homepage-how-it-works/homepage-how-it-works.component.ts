import { Component, OnInit } from "@angular/core";
import { environment } from "./../../../environments/environment";

@Component({
  selector: "app-homepage-how-it-works",
  templateUrl: "./homepage-how-it-works.component.html",
  styleUrls: ["./homepage-how-it-works.component.scss"]
})
export class HomepageHowItWorksComponent implements OnInit {
  urlBase: string = environment.urlBase;
  constructor() {}

  ngOnInit() {}
}
