import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageFrontMessageComponent } from './homepage-front-message.component';

describe('HomepageFrontMessageComponent', () => {
  let component: HomepageFrontMessageComponent;
  let fixture: ComponentFixture<HomepageFrontMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepageFrontMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageFrontMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
