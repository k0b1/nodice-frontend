import { Component, OnInit } from '@angular/core';
import { environment } from "./../../../environments/environment";

@Component({
  selector: 'app-homepage-front-message',
  templateUrl: './homepage-front-message.component.html',
  styleUrls: ['./homepage-front-message.component.scss']
})
export class HomepageFrontMessageComponent implements OnInit {
  urlBase: string = environment.urlBase;

  constructor() { }

  ngOnInit() {
  }

}
