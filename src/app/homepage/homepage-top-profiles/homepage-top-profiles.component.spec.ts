import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageTopProfilesComponent } from './homepage-top-profiles.component';

describe('HomepageTopProfilesComponent', () => {
  let component: HomepageTopProfilesComponent;
  let fixture: ComponentFixture<HomepageTopProfilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepageTopProfilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageTopProfilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
