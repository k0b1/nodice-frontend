import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-homepage-top-profiles",
  templateUrl: "./homepage-top-profiles.component.html",
  styleUrls: ["./homepage-top-profiles.component.scss"]
})
export class HomepageTopProfilesComponent implements OnInit {
  @Input() profiles: any[];
  constructor() {}

  ngOnInit() {}
}
