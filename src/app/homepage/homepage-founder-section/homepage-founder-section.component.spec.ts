import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageFounderSectionComponent } from './homepage-founder-section.component';

describe('HomepageFounderSectionComponent', () => {
  let component: HomepageFounderSectionComponent;
  let fixture: ComponentFixture<HomepageFounderSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepageFounderSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageFounderSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
