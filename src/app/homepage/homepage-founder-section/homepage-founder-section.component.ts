import { Component, OnInit } from "@angular/core";
import { environment } from "./../../../environments/environment";

@Component({
  selector: "app-homepage-founder-section",
  templateUrl: "./homepage-founder-section.component.html",
  styleUrls: ["./homepage-founder-section.component.scss"]
})
export class HomepageFounderSectionComponent implements OnInit {
  urlBase: string = environment.urlBase;

  constructor() {}

  ngOnInit() {}
}
