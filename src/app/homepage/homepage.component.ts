import { Component, OnInit } from "@angular/core";

//import { HeaderHomeComponent } from "../../header/header.component";
//import { FooterComponent } from "../../footer/footer.component";
import { GetProfilesService } from "../services/get-from-database/get-profiles.service";
import { GetCommentsService } from "../services/get-from-database/get-comments.service";
import { environment } from "./../../environments/environment";

@Component({
  selector: "app-homepage",
  templateUrl: "./homepage.component.html",
  styleUrls: ["./homepage.component.scss"]
})
export class HomepageComponent implements OnInit {
  ifActiveDotGoDarkOne = "";
  ifActiveDotGoDarkTwo = "";
  ifActiveDotGoDarkThree = "";
  urlBase: string = environment.urlBase;

  ratingsObject: {} = {};
  finalGrade: number = 0;
  numberOfCustomerComments: number = 0;

  profiles; //container for GET profiles from database
  constructor(
    private getProfiles: GetProfilesService,
    private getComments: GetCommentsService
  ) {}

  ngOnInit() {
    this.getProfilesFromDatabase();
  }

  makeActiveFirst() {}
  makeActiveSecond() {}
  makeActiveThird() {}

  getProfilesFromDatabase() {
    this.getProfiles.getProfiles().subscribe(response => {
      this.profiles = response;

      for (let profile in this.profiles) {
        this.getProfiles
          .getProfileImage(this.profiles[profile].author)
          .subscribe(response => {
            console.log(response);
            if (response) {
              if (response[0] != null) {
                this.profiles[profile].profile_image = response[0].source_url;
              }
            } else {
              this.profiles[profile].profile_image = this.profiles[
                profile
              ].featured_image_src;
            }
          });
      }
    });
  }
}
