import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThankYouCustomerComponent } from './thank-you-customer.component';

describe('ThankYouCustomerComponent', () => {
  let component: ThankYouCustomerComponent;
  let fixture: ComponentFixture<ThankYouCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThankYouCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThankYouCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
