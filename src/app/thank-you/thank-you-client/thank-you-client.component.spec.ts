import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThankYouClientComponent } from './thank-you-client.component';

describe('ThankYouClientComponent', () => {
  let component: ThankYouClientComponent;
  let fixture: ComponentFixture<ThankYouClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThankYouClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThankYouClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
