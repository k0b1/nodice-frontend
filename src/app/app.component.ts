import { Component, OnInit } from "@angular/core";

import { HeaderHomeComponent } from "./shared/layout/header/header.component";
import { HomepageComponent } from "./homepage/homepage.component";
import { FooterComponent } from "./shared/layout/footer/footer.component";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  title = "nodice";
  nonceInLocalStorage: string;
  isLoggedInLocalStorage: string;

  ngOnInit() {
    this.nonceInLocalStorage = localStorage.getItem("user");
    this.isLoggedInLocalStorage = localStorage.getItem("isUserLoggedIn");

    console.log(
      "app comp",
      this.nonceInLocalStorage,
      this.isLoggedInLocalStorage
    );
  }
}
