import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { SlickCarouselModule } from 'ngx-slick-carousel';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { HeaderHomeComponent } from './shared/layout/header/header.component';
import { FooterComponent } from './shared/layout/footer/footer.component';
import { BodyComponent } from './shared/layout/body/body.component';
import { CategoriesComponent } from './categories/categories.component';
import { ProfileComponent } from './profile/profile.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SignupComponent } from './auth/signup/signup.component';
import { ThankYouComponent } from './thank-you/thank-you.component';
import { DashboardGeneralClientComponent } from './dashboard/client/dashboard-general-client/dashboard-general-client.component';
import { DashboardGalleryComponent } from './dashboard/client/dashboard-gallery/dashboard-gallery.component';
import { DashboardPlanComponent } from './dashboard/client/dashboard-plan/dashboard-plan.component';
import { DashboardBillingComponent } from './dashboard/client/dashboard-billing/dashboard-billing.component';
import { DashboardGeneralComponent } from './dashboard/customer/dashboard-general/dashboard-general.component';
import { DashboardReviewsComponent } from './dashboard/customer/dashboard-reviews/dashboard-reviews.component';
import { LoginComponent } from './auth/login/login.component';
import { ThankYouCustomerComponent } from './thank-you/thank-you-customer/thank-you-customer.component';
import { ThankYouClientComponent } from './thank-you/thank-you-client/thank-you-client.component';
import { DeleteImageComponent } from './popups/delete-image/delete-image.component'
import { ManipulateTextService } from './services/write-to-database/manipulate-client-text.service';
import { DashboardNavigationClientComponent } from './dashboard/client/dashboard-navigation-client/dashboard-navigation-client.component';
import { AuthenticateService } from './services/authenticate/authenticate.service';
import { HeaderDashboardComponent } from './shared/layout/header/header-dashboard/header-dashboard.component';
import { DashboardNavigationCustomerComponent } from './dashboard/customer/dashboard-navigation-customer/dashboard-navigation-customer.component';
import { HeaderSearchComponent } from './shared/layout/header/header-search/header-search.component';
import { GiveStarsComponent } from './give-stars/give-stars.component';

import { DragDropModule } from '@angular/cdk/drag-drop';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { DeleteStripeCustomerComponent } from './popups/delete-stripe-customer/delete-stripe-customer.component';
import { SidebarComponent } from './shared/layout/header/sidebar/sidebar.component';

import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import { DisplayServicesComponent } from './display-services/display-services.component';
import { ProfileCommentsComponent } from './profile/profile-comments/profile-comments.component';
import { ProfileGiveRatingComponent } from './profile/profile-give-rating/profile-give-rating.component';
import { ProfileCarouselComponent } from './profile/profile-carousel/profile-carousel.component';
import { ProfileDescriptionComponent } from './profile/profile-description/profile-description.component';
import { ProfileServicesComponent } from './profile/profile-services/profile-services.component';
import { ProfileContactInformationComponent } from './profile/profile-contact-information/profile-contact-information.component';
import { HomepageTopProfilesComponent } from './homepage/homepage-top-profiles/homepage-top-profiles.component';
import { HomepageFounderSectionComponent } from './homepage/homepage-founder-section/homepage-founder-section.component';
import { HomepageHowItWorksComponent } from './homepage/homepage-how-it-works/homepage-how-it-works.component';
import { HomepageFrontMessageComponent } from './homepage/homepage-front-message/homepage-front-message.component';
import { AuthGuard } from './auth-guard.service';
import { ReviewsResultComponent } from './shared/shared-components/reviews-result/reviews-result.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    HeaderHomeComponent,
    FooterComponent,
    BodyComponent,
    CategoriesComponent,
    ProfileComponent,
    SearchPageComponent,
    DashboardComponent,
    SignupComponent,
    ThankYouComponent,
    DashboardGeneralClientComponent,
    DashboardGalleryComponent,
    DashboardPlanComponent,
    DashboardBillingComponent,
    DashboardGeneralComponent,
    DashboardReviewsComponent,
    LoginComponent,
    ThankYouCustomerComponent,
    ThankYouClientComponent,
    DashboardNavigationClientComponent,
    HeaderDashboardComponent,
    DashboardNavigationCustomerComponent,
    HeaderSearchComponent,
    GiveStarsComponent,
    DeleteImageComponent,
    DeleteStripeCustomerComponent,
    SidebarComponent,
    DisplayServicesComponent,
    ProfileCommentsComponent,
    ProfileGiveRatingComponent,
    ProfileCarouselComponent,
    ProfileDescriptionComponent,
    ProfileServicesComponent,
    ProfileContactInformationComponent,
    HomepageTopProfilesComponent,
    HomepageFounderSectionComponent,
    HomepageHowItWorksComponent,
    HomepageFrontMessageComponent,
    ReviewsResultComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SlickCarouselModule,
    HttpClientModule,
    DragDropModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    // MaterialModule,
    MatSelectModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatInputModule

  ],
  entryComponents: [
    DeleteImageComponent,
    DeleteStripeCustomerComponent
  ],
  providers: [
    ManipulateTextService,
      AuthenticateService,
      AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
