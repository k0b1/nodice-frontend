import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthenticateService } from './authenticate/authenticate.service';

import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BlockOtherUsersDashboardService {
  
  savedNonce: string = null;
  isLoggedInLocalStorage: string;

  notYourPage: boolean = false; //if someone change URL to see other clients dashboard page
  baseUrl: string = environment.apiHost;

  constructor( private route: ActivatedRoute, private authService: AuthenticateService) { }

  blockOtherUsersDashboard( routeID: string ) {
    this.savedNonce = localStorage.getItem('user');
    this.isLoggedInLocalStorage = localStorage.getItem('isUserLoggedIn');

    /** check allowed pages */
    this.authService.authenticateUser( this.savedNonce, this.isLoggedInLocalStorage ).subscribe( response => {
     
      if ( response.ID != routeID ) {
        this.notYourPage = true;
        //navigate to error page
      }
    });
  }
}
