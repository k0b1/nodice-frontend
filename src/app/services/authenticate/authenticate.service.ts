import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class AuthenticateService {
  /** Endpoints */
  // baseUrl: string = "http://localhost/svetRada/nodice/wp-json/wp/v2";
  // baseUrl: string = "http://nodice.gnf.dk/wp-json/wp/v2";
  baseUrl: string = environment.apiHost;
  urlForClientsRegistering: string = this.baseUrl + "/users/register_clients";
  urlForCustomersRegistering: string =
    this.baseUrl + "/users/register_customers";
  urlLoginUsers: string = this.baseUrl + "/login";
  urlLogoutUser: string = this.baseUrl + "/logout";

  urlAuthenticateUser: string = this.baseUrl + "/authenticate_user";
  urlCreatePage: string = this.baseUrl + "/create_new_profile_page";
  /** End of endpoints. */

  authUser: {};

  constructor(private httpClient: HttpClient) {}

  public registerClients(obj: {}): Observable<any> {
    return this.httpClient
      .post(this.urlForClientsRegistering, obj, {
        headers: new HttpHeaders({
          "Content-Type": "application/json"
        })
      })
      .pipe(map(data => data));
  }
  public registerCustomers(obj: {}): Observable<any> {
    return this.httpClient
      .post(this.urlForCustomersRegistering, obj, {
        headers: new HttpHeaders({
          "Content-Type": "application/json"
        })
      })
      .pipe(map(data => data));
  }

  public loginUsers(obj: {}): Observable<any> {
    return this.httpClient
      .post(this.urlLoginUsers, obj, {
        headers: new HttpHeaders({
          "Content-Type": "application/json"
        })
      })
      .pipe(map(data => data));
  }

  authenticateUserObject: {
    authenticateUser: string;
    authenticateNonce: string;
    userID: string;
    displayName: string;
  } = {
    authenticateUser: localStorage.getItem("isUserLoggedIn"),
    authenticateNonce: localStorage.getItem("user"),
    userID: localStorage.getItem("userID"),
    displayName: localStorage.getItem("displayName")
  };

  isLoggedIn: boolean = false;
  isLoggedInLocalStorage: string = "false";
  /**
   *
   * Authentication.
   * @param auth
   *
   */

  /** method that "asks" wp rest endpoint if user is logged in, and if nonce is verified */
  public authenticateUser(auth, isLoggedLocalStorage): Observable<any> {
    if (!this.isLoggedIn) {
      if (isLoggedLocalStorage == "true") {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    }
    if (this.isLoggedIn) {
      this.authenticateUserObject.authenticateNonce = auth;
      if (auth) {
        return this.httpClient.get(
          this.urlAuthenticateUser +
            "?_wpnonce=" +
            this.authenticateUserObject.authenticateNonce
        );
      }
    }
  }

  /**  logout user */
  public logoutUser(authNonce: string): Observable<any> {
    this.authenticateUserObject.authenticateNonce = "";
    this.authenticateUserObject.authenticateUser = "";
    this.authenticateUserObject.userID = "";
    this.authenticateUserObject.displayName = "";
    localStorage.setItem("isUserLoggedIn", "false");
    localStorage.setItem("user", null);
    localStorage.setItem("userID", "");
    localStorage.setItem("displayName", "");

    if (this.isLoggedIn) {
      return this.httpClient.get(this.urlLogoutUser + "?_wpnonce=" + authNonce);
    }
  }

  public createPageForNewClient(
    auth: string,
    pagename: string,
    categories: []
  ) {
    let categoriesArray = categories;

    let params = new HttpParams();
    params = params.append("categories", categoriesArray.join(", "));
    // this.http.get(url, { params: params });

    return this.httpClient.get(
      this.urlCreatePage +
        "?_wpnonce=" +
        auth +
        "&pagename=" +
        pagename +
        "&" +
        params
    );
  }

  /**
   * @param nonce: created by Wordpress.
   * @param nonceLocalStorage: WP nonce saved to Local Storage, used to remember app state on page refresh */
  userData: {
    ID;
    authenticate;
    role;
  };
  public checkIfUserIsLoggedIn(
    nonce: string,
    nonceLocalStorage: string,
    isUserLoggedInLocalStorage
  ) {
    this.isLoggedInLocalStorage = localStorage.getItem("isUserLoggedIn");

    if (!nonce) {
      nonce = nonceLocalStorage;
    }

    // console.log('isUserLoggedIn', this.isLoggedInLocalStorage );
    if (!this.isLoggedIn) {
      if (isUserLoggedInLocalStorage == "true") {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    }

    if (nonce && this.isLoggedIn) {
      this.authenticateUser(nonce, isUserLoggedInLocalStorage).subscribe(
        response => {
          this.isLoggedIn = true;
          this.userData = response;
        }
      );
    } else {
      this.isLoggedIn = false;
      this.userData = { ID: "", authenticate: "false", role: "" };
      // return false;
    }
    return this.userData;
  }

  /** take data/text from edited Client profile text box and send it to WP database */
  public saveClientText(text: string) {
    return this.httpClient
      .post(this.urlForCustomersRegistering, text, {
        headers: new HttpHeaders({
          "Content-Type": "application/json"
        })
      })
      .pipe(map(data => data));
  }
}
