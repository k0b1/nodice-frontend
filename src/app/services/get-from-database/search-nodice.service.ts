import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class SearchNodiceService {
  baseUrl: string = environment.apiHost;

  constructor(private httpClient: HttpClient) {}

  searchNodice(serviceID, categoryID, ratings) {
    let serviceParam: string = "";
    let categoryParam: string = "";
    let ratingsParam: string = "";
    if (serviceID == "0" || serviceID == "") {
      serviceParam = "";
    } else {
      serviceParam = "services=" + serviceID;
    }
    if (categoryID !== "0") {
      categoryParam = "&categories=" + categoryID;
    }
    if (ratings !== "0" || ratings !== "") {
      ratingsParam = "&ratings=" + ratings;
    }
    return this.httpClient.get(
      this.baseUrl + "/pages/?" + serviceParam + categoryParam + ratingsParam
    );
  }
}
