import { TestBed } from '@angular/core/testing';

import { GetProfilesService } from './get-profiles.service';

describe('GetProfilesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetProfilesService = TestBed.get(GetProfilesService);
    expect(service).toBeTruthy();
  });
});
