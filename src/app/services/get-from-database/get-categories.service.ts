import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GetCategoriesService {
 // baseUrl: string = "http://nodice.gnf.dk/wp-json/wp/v2/categories";
//  baseUrl: string = "http://localhost/svetRada/nodice/wp-json/wp/v2/categories";
baseUrl: string = environment.apiHost + "/categories";
getPages : string = environment.apiHost;
// urlForClientsRegistering:string =  this.baseUrl + "/users/register_clients";



  constructor( private httpClient: HttpClient ) { }

  getCategoriesFromDatabase() {
    return this.httpClient.get<[]>(this.baseUrl );
  }

  /** get pages by category id */
  getPagesForCategory( categoryID ) {
    return this.httpClient.get<any[]>(this.getPages+"pages/?categories="+categoryID)
  }
}
