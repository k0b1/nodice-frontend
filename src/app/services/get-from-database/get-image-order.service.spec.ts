import { TestBed } from '@angular/core/testing';

import { GetImageOrderService } from './get-image-order.service';

describe('GetImageOrderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetImageOrderService = TestBed.get(GetImageOrderService);
    expect(service).toBeTruthy();
  });
});
