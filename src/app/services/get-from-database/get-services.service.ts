import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetServicesService {
  
  baseUrl: string = environment.apiHost;
  urlPage: string = environment.apiHost + "/pages/";
  
  constructor( private httpClient: HttpClient) { }

  getServicesFromDatabase() {
    return this.httpClient.get( this.baseUrl + "/services" );
  }

  displayChosenServices( pageID ) {
    return this.httpClient.get( this.urlPage + pageID );
  }
}
