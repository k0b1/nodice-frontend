import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GetImageOrderService {

  // baseUrl: string = "http://nodice.gnf.dk/wp-json/wp/v2";
  // baseUrl: string = "http://localhost/svetRada/nodice/wp-json/wp/v2";
  baseUrl: string = environment.apiHost;
  
  urlGetImageOrder: string = this.baseUrl + "/get-image-order";

  constructor( private httpClient: HttpClient ) { }

  getImageOrder( nonce ) {
    return this.httpClient.get<any[]>( this.urlGetImageOrder + "?_wpnonce=" + nonce );
  }

}
