import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GetClientDataService {

  // baseUrl: string = "http://localhost/svetRada/nodice/wp-json/wp/v2";
  baseUrl: string = environment.apiHost;
  urlGetUserData: string = this.baseUrl + "/get_user_data";
  
  constructor( private httpClient: HttpClient ) { }

  getClientData(nonce) {
    return this.httpClient.get<any[]>( this.urlGetUserData +"?_wpnonce="+nonce );
  }

  displayClientContactDataOnProfilePage( clientID ) {
    return this.httpClient.get<any[]>( this.urlGetUserData + "?client_id=" + clientID );
  }
}
