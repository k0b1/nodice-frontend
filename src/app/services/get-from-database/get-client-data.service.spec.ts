import { TestBed } from '@angular/core/testing';

import { GetClientDataService } from './get-client-data.service';

describe('GetClientDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetClientDataService = TestBed.get(GetClientDataService);
    expect(service).toBeTruthy();
  });
});
