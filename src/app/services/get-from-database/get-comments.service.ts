import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { PostCommentService } from "../write-to-database/post-comment.service";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class GetCommentsService {
  // baseUrl: string = "http://nodice.gnf.dk/wp-json/wp/v2/categories";
  //  baseUrl: string = "http://localhost/svetRada/nodice/wp-json/wp/v2";
  baseUrl: string = environment.apiHost;

  urlGetComment: string = this.baseUrl + "/comments";
  urlGetRatings: string = this.baseUrl + "/get-ratings";
  urlCalculateRatings: string = this.baseUrl + "/calculate-ratings";

  //  sumOfStars: number = 0;
  // numberOfCustomerComments: number = 0;
  // finalGrade: number = 0;

  constructor(
    private httpClient: HttpClient,
    private postReview: PostCommentService
  ) {}

  getComments(pageID) {
    return this.httpClient.get<[]>(
      this.urlGetComment + "?per_page=100&post=" + pageID
    );
  }

  getCommentsRatings(allComments) {
    for (let comment of allComments) {
      this.getRatings(comment.comment_ID || comment.id).subscribe(response => {

        /** create array which length is based on rating grade, for each comment */
        comment.rating = [...Array(Number(response))].map(x => 0);
      });
    }
  }

  getRatings(commentID) {
    return this.httpClient.post(this.urlGetRatings, { commentID });
  }

  getCommentsByUser(customerID, nonce) {
    return this.httpClient.get(
      this.baseUrl +
        "/list-comments?per_page=100&author=" +
        customerID +
        "&_wpnonce=" +
        nonce
    );
  }

  getPageTitleOfComments(pageID) {
    return this.httpClient.get(
      this.baseUrl + "/get-page-title?page_id=" + pageID
    );
  }
}
