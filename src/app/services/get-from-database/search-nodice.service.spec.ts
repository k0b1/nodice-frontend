import { TestBed } from '@angular/core/testing';

import { SearchNodiceService } from './search-nodice.service';

describe('SearchNodiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchNodiceService = TestBed.get(SearchNodiceService);
    expect(service).toBeTruthy();
  });
});
