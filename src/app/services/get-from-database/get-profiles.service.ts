import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GetProfilesService {

 // baseUrl: string = "http://nodice.gnf.dk/wp-json/wp/v2/categories";
//  baseUrl = "http://localhost/svetRada/nodice/wp-json/wp/v2";
baseUrl: string = environment.apiHost;

 baseUrlPages: string = this.baseUrl + "/pages";
 urlGetProfileImage: string = this.baseUrl + "/get_profile_image"; 
 constructor( private httpClient: HttpClient) { }

 getProfiles() {
   return this.httpClient.get( this.baseUrlPages );
 }

 getProfileImage( userID ) {
   return this.httpClient.post( this.urlGetProfileImage, userID ); 
 }
}
