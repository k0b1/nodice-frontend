import { Injectable } from '@angular/core';
import { AuthenticateService } from './authenticate/authenticate.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LogoutService {

  constructor( private authUser: AuthenticateService, private router: Router ) { }
  
  /** as there are three headers, this method controls all three logout actions
   * @params isLoggedInLocalStorage: "true" or "false", save in Local Storage info about user logg status
   * @params appStateNonce: WP nonce, saved on first login - current Angular state before page refreshing
   * @params localeStorageNonce: WP nonce saved in LocalStorage
   */

  onLogout( isLoggedInLocalStorage, appStateNonce, localeStorageNonce:string ){
    isLoggedInLocalStorage = "false";
    if ( !appStateNonce ) {
      localeStorageNonce = localStorage.getItem('user');
      
    }
    
    
    this.authUser.logoutUser(localeStorageNonce).subscribe( (response) => {
      
      if ( response.logout == "yes" ) {
        

        this.authUser.authenticateUserObject.authenticateNonce = "";
        localeStorageNonce = "";
        localStorage.removeItem('user');
        this.authUser.isLoggedIn = false;
        localStorage.setItem('isUserLoggedIn', 'false');
        isLoggedInLocalStorage = "false";

        // window.location.reload();
        this.router.navigate(['/login']);

        // this.redirectTo('/');
      } else if ( response.logout == "no" ) {
        
        localStorage.setItem('user', "");
        localStorage.setItem('isUserLoggedIn', "false");
        this.router.navigate(['/login']);
      }
    } )
  
  }
  /** probably delete later */
  redirectTo(uri:string){
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
    this.router.navigate([uri]));}
}
