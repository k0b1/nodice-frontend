import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EmailValidator } from '@angular/forms';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SaveClientContactDataService {
  // baseUrl: string = "http://localhost/svetRada/nodice/wp-json/wp/v2";
  baseUrl: string = environment.apiHost;

  urlSaveUserData: string = this.baseUrl + "/save_user_data";
  constructor( private httpClient: HttpClient) { }

  saveUserData( nonce, userData: { phone, email, website } ) {
    return this.httpClient.post(this.urlSaveUserData+"?_wpnonce="+nonce,userData )
  }

}
