import { TestBed } from '@angular/core/testing';

import { SaveServicesService } from './save-services.service';

describe('SaveServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaveServicesService = TestBed.get(SaveServicesService);
    expect(service).toBeTruthy();
  });
});
