import { TestBed } from '@angular/core/testing';

import { SaveClientContactDataService } from './save-client-contact-data.service';

describe('SaveClientContactDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaveClientContactDataService = TestBed.get(SaveClientContactDataService);
    expect(service).toBeTruthy();
  });
});
