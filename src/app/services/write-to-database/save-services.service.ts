import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SaveServicesService {
  
  baseUrl: string = environment.apiHost;

  constructor( private httpClient: HttpClient) { }

  saveServicesForPage( pageID, servicesArray) {
    
    return this.httpClient.post( this.baseUrl + "/save_services", { pageID: pageID, services: servicesArray } )
  }
}
