import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';;

import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ManipulateTextService {
  // baseUrl: string = "http://nodice.gnf.dk/wp-json/wp/v2";
  // baseUrl: string = "http://localhost/svetRada/nodice/wp-json/wp/v2";
  baseUrl: string = environment.apiHost;
  
  // urlForClientsRegistering:string =  this.baseUrl + "/users/register_clients";
  private urlSaveClientData: string = this.baseUrl + "/pages/save_user_profile_text";
  private urlGetClientData : string = this.baseUrl + "/pages";
  
  userObject : any = {
    postID: '',
    postAuthor: '',
    postContent: '',
    postTitle: '',
    postExcerpt: '',
    postStatus: '',
    postComments: 'open',
    postCategories: []
  }
  constructor( private httpClient: HttpClient) { }




   /** take data/text from edited Client profile text box and send it to WP database */
   public saveClientText( userObject: {}, nonce: string ) {

    let paramsSavePageText = new HttpParams()
    .append('_wpnonce', nonce);

    return this.httpClient.post(this.urlSaveClientData, userObject, {
      headers: new HttpHeaders({
           'Content-Type':  'application/json',
         }),
      params: paramsSavePageText
    }).pipe( map(data=>
     data) );
  }

  public getClientText( userID: string) {
    return this.httpClient.get<any[]>( this.urlGetClientData, {
      params: {
        author: userID
      }
    });
  }
}
