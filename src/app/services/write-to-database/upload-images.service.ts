import { Injectable } from "@angular/core";

import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpEvent,
  HttpRequest
} from "@angular/common/http";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { AuthenticateService } from "../authenticate/authenticate.service";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class UploadImagesService {
  baseUrl: string = environment.apiHost;

  urlUploadImage: string = this.baseUrl + "/upload-image";
  urlGetImages: string = this.baseUrl + "/media";
  urlGetPage: string = this.baseUrl + "/pages";
  urlUploadOrder: string = this.baseUrl + "/upload_order";
  urlDeleteImage: string = this.baseUrl + "/delete_image";

  nonce: string;
  isAuthenticated: boolean = false;

  constructor(
    private httpClient: HttpClient,
    private authUser: AuthenticateService
  ) {}

  public uploadImage(url: string, file: File): Observable<any> {
    let formData = new FormData();
    formData.append("file", file);

    /** authenticate user */
    if (!this.authUser.authenticateUserObject.authenticateNonce) {
      this.nonce = localStorage.getItem("user"); //get nonce from localstorage
    } else {
      this.nonce = this.authUser.authenticateUserObject.authenticateNonce;
    }
    let authenticate = new HttpParams().append("_wpnonce", this.nonce);
    console.log("auth", authenticate);
    /** lost whole day trying to send post request with headers and at the end it seems that headers were the things that were blocking proper sending. life is beautiful. */

    return this.httpClient.post(url, formData, {
      params: authenticate,
      reportProgress: true
    });
  }

  public uploadImageOrder(order: any[], nonce: string) {
    return this.httpClient.post(
      this.urlUploadOrder + "?_wpnonce=" + nonce,
      order
    );
  }

  public getClientImages(userID: string) {
    return this.httpClient.get<any[]>(this.urlGetImages, {
      params: {
        author: userID
      }
    });
  }

  public getImagesByPagename(pagename) {
    return this.httpClient.get<any[]>(this.urlGetPage, {
      params: {
        slug: pagename
      }
    });
  }

  /** */
  public deleteImagesOnClick(imageID: string, auth: string) {
    return this.httpClient.get<any[]>(
      this.urlDeleteImage +
        "?image_id=" +
        imageID +
        "&_wpnonce=" +
        auth +
        "&confirm=true"
    );
  }
}
