import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { map } from "rxjs/operators";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class PostCommentService {
  // baseUrl: string = "http://nodice.gnf.dk/wp-json/wp/v2";
  //  baseUrl: string = "http://localhost/svetRada/nodice/wp-json/wp/v2";
  baseUrl: string = environment.apiHost;
  urlComments: string = this.baseUrl + "/comments";
  urlRatings: string = this.baseUrl + "/rate-profile";

  constructor(private httpClient: HttpClient) {}

  postComment(obj) {
    let nonceInLocalStorage = localStorage.getItem("user");
    let commentParams = new HttpParams()
      .append("author", obj.author)
      .append("author_name", obj.author_name)
      .append("content", obj.content)
      .append("_wpnonce", nonceInLocalStorage);

    return this.httpClient
      .post(this.urlComments, obj, {
        headers: new HttpHeaders({
          "Content-Type": "application/json"
        }),
        params: commentParams
      })
      .pipe(map(data => data));
  }

  rateProfile(customerID, commentID, rating) {
    let obj = {
      customerID,
      commentID,
      rating
    };
    return this.httpClient.post(this.urlRatings, obj);
  }
}
