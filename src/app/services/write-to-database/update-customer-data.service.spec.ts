import { TestBed } from '@angular/core/testing';

import { UpdateCustomerDataService } from './update-customer-data.service';

describe('UpdateCustomerDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UpdateCustomerDataService = TestBed.get(UpdateCustomerDataService);
    expect(service).toBeTruthy();
  });
});
