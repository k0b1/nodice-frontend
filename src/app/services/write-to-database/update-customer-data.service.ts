import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UpdateCustomerDataService {
  // baseUrl: string = "http://nodice.gnf.dk/wp-json/wp/v2";
  // baseUrl: string = "http://localhost/svetRada/nodice/wp-json/wp/v2";
  baseUrl: string = environment.apiHost;
  
  urlUpdateCustomer = this.baseUrl + "/update-customer";
  urlUpdateCustomerPassword = this.baseUrl + "/update-customer-password";
  constructor( private httpClient: HttpClient ) { }

  updateUserData( obj: {} ) {
    return this.httpClient.post( this.urlUpdateCustomer, obj );
  }

  updatePassword( obj: {}, wpnonce ) {
    return this.httpClient.post( this.urlUpdateCustomerPassword+"?_wpnonce="+wpnonce, obj );
  }
}
