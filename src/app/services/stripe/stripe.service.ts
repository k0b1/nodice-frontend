import { Injectable, ɵɵNgOnChangesFeature } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StripeService {
  // baseUrl: string = "http://nodice.gnf.dk/wp-json/wp/v2";
  // baseUrl: string = "http://localhost/svetRada/nodice/wp-json/wp/v2";
  baseUrl: string = environment.apiHost;

  urlCreateStripeProduct = this.baseUrl + '/create_stripe_product';
  urlAttachPlanToProduct = this.baseUrl + '/attach_plan_to_product';
  urlStripeBilling: string = this.baseUrl + '/stripe_billing';
  urlAttachCustomerToThePlan = this.baseUrl + '/attach_customer_to_the_plan';
  urlDeleteStripeCustomer = this.baseUrl + "/delete_stripe_customer";
  urlSaveStripePlan: string = this.baseUrl + '/save_stripe_plan';
  urlGetStripePlan: string = this.baseUrl + '/get_stripe_plan';
  urlGetBillingHistory: string = this.baseUrl + "/get_billing_history";

  constructor(
    private httpClient: HttpClient
  ) { }


  // public sendStripeToken( token ) {
  //   return this.httpClient.post( this.urlStripeBilling, token );
  // }

  /** create Stripe Products: Popular, Social Butterfly, Influencer */
  public createStripeProduct( type: string ) {
    return this.httpClient.post( this.urlCreateStripeProduct, type );
  }

  public attachPlanToProduct() {
    return this.httpClient.post( this.urlAttachPlanToProduct, {} );
  }

  public startBillingCustomer( { token, planID }, nonce) {
    return this.httpClient.post( this.urlStripeBilling+"?_wpnonce="+nonce, { token, planID } );

  }

  public attach_customer_to_the_plan( token, nonce, activePlan ){
    return this.httpClient.post( this.urlAttachCustomerToThePlan + "?_wpnonce=" + nonce, { token, activePlan } );
  }

  public delete_stripe_client( nonce: string, userID ) {
    return this.httpClient.post( this.urlDeleteStripeCustomer+"?_wpnonce=" + nonce, userID );
  }

  public saveStripePlan( nonce, plan ) {
    return this.httpClient.get( this.urlSaveStripePlan+"?_wpnonce=" + nonce + "&chosen_plan=" + plan );
  }


  public getStripePlan( nonce ) {
    return this.httpClient.get( this.urlGetStripePlan+"?_wpnonce=" + nonce );
  }

  public getBillingHistory( nonce, userID ) {
    return this.httpClient.get( this.urlGetBillingHistory + "?_wpnonce=" + nonce + "&user_id=" + userID.userID );
  }
}
