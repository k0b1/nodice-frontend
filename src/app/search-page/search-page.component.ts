import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { HeaderSearchComponent } from "../shared/layout/header/header-search/header-search.component";
import { SearchNodiceService } from "./../services/get-from-database/search-nodice.service";
import { GetProfilesService } from "./../services/get-from-database/get-profiles.service";

@Component({
  selector: "app-search-page",
  templateUrl: "./search-page.component.html",
  styleUrls: ["./search-page.component.scss"]
})
export class SearchPageComponent implements OnInit {
  serviceID: string;
  categoryID: string;
  ratingsValue: string;

  searchResults: any;
  constructor(
    private search: SearchNodiceService,
    private route: ActivatedRoute,
    private getProfileImage: GetProfilesService
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe(response => {
      console.log(response);
      this.serviceID = response.services;
      this.categoryID = response.category;
      this.ratingsValue = response.ratings;
      this.searchNodice();
      // console.log( this.ratingsValue );
    });
  }

  searchNodice() {
    this.search
      .searchNodice(this.serviceID, this.categoryID, this.ratingsValue)
      .subscribe(response => {
        console.log("results found", response);
        this.searchResults = response;
        for (let page in this.searchResults) {
          console.log(" id's ", this.searchResults[page].author);
          this.getProfileImage
            .getProfileImage(this.searchResults[page].author)
            .subscribe(response => {
              if (response) {
                console.log(response[0]);
                this.searchResults[page].profile_image = response[0].source_url;
                // console.log('addition: ', this.profiles );
              } else {
                this.searchResults[page].profile_image = this.searchResults[
                  page
                ].featured_image_src;
              }
              // console.log( "profile image : ", response[0] );
            });
        }
      });
  }
}
