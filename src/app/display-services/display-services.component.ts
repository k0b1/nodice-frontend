import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { GetServicesService } from '../services/get-from-database/get-services.service';

@Component({
  selector: 'app-display-services',
  templateUrl: './display-services.component.html',
  styleUrls: ['./display-services.component.scss']
})
export class DisplayServicesComponent implements OnInit, OnChanges {

  @Input() pageID;
  
  allServices: any = [];
  displayChosenServices : any = [];

  constructor( private getServices: GetServicesService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (typeof changes['pageID'] !== "undefined") {
  
        var change = changes['pageID'];
        this.pageID = change.currentValue;

        if ( this.pageID !== undefined ) { 
          this.getServicesFromDatabase(this.pageID);
        }
    }
  }
  
  getServicesFromDatabase( pageID) {
    this.getServices.getServicesFromDatabase().subscribe( response => {
      
      this.allServices = response;

       /** display nodice services data for current page */
       this.getServices.displayChosenServices( pageID ).subscribe( response => {

        for ( let service in this.allServices ) {
          for ( let serviceID in response['services'] ) {
            if ( this.allServices[service].id == response['services'][serviceID] ) {

              this.displayChosenServices.push( this.allServices[service].name )
            }
          }
          
        }
        
      })
    })
  }

}
