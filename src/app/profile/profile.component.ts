import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { AuthenticateService } from "../services/authenticate/authenticate.service";
import { ActivatedRoute } from "@angular/router";

import { UploadImagesService } from "../services/write-to-database/upload-images.service";
import { PostCommentService } from "../services/write-to-database/post-comment.service";
import { NgForm } from "@angular/forms";
import { GetCommentsService } from "../services/get-from-database/get-comments.service";
import { GetClientDataService } from "../services/get-from-database/get-client-data.service";
import { GetServicesService } from "../services/get-from-database/get-services.service";
import { HeaderDashboardComponent } from "./../shared/layout/header/header-dashboard/header-dashboard.component"

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"]
})
export class ProfileComponent implements OnInit {
  // title = 'ngSlick';

  // authUser: boolean;
  businessPageName: string = "";

  pageData: any[];
  getImagesData: any[];

  customerIsLoggedIn: boolean = false;
  isLoggedInLocalStorage: string;

  customerID: string;
  pageID: number;
  customerName: string;
  listAllComments: any[];

  clientID: string;

  /** contact data */
  contactData: Object = {
    phone: "",
    email: "",
    website: ""
  };

  /** services */
  allServices: any;
  displayChosenServices: any = [];
  /** stars variables */
  sumOfStars: number = 0;
  finalGrade: number = 0;
  numberOfCustomerComments: number = 0;

  @ViewChild("stars", { static: false }) stars: ElementRef;
  @ViewChild("rating", { static: false }) rating: ElementRef;

  constructor(
    private authenticateUser: AuthenticateService,
    private route: ActivatedRoute,
    private getImages: UploadImagesService,
    private getComments: GetCommentsService,
    private getUserData: GetClientDataService
  ) {}

  ngOnInit() {
    this.businessPageName = this.route.snapshot.params["business-page-name"];
    this.getProfileData();

    // logged in user
    if (
      this.authenticateUser.authenticateUserObject.authenticateUser == "true"
    ) {
      this.customerIsLoggedIn = true;
      this.customerID = this.authenticateUser.authenticateUserObject.userID;
      this.customerName = this.authenticateUser.authenticateUserObject.displayName;
    }
  }

  authenticateUserProfilePage() {
    return this.authenticateUser
      .authenticateUser(
        this.authenticateUser.authenticateUserObject,
        this.isLoggedInLocalStorage
      )
      .subscribe(response => {});
  }

  getProfileData() {
    this.getImages
      .getImagesByPagename(this.businessPageName)
      .subscribe(response => {
        this.clientID = response[0].author;
        this.getUserData
          .displayClientContactDataOnProfilePage(this.clientID)
          .subscribe(response => {
            this.contactData["phone"] = response["phone"];
            this.contactData["email"] = response["email"];
            this.contactData["website"] = response["website"];
          });

        this.pageID = response[0].id;
        this.getCommentsFromDatabase(this.pageID);

        this.pageData = response[0];
        if (response.length > 0) {
          /** page found, continue */
          this.getImages
            .getClientImages(response[0].author)
            .subscribe(response => {
              console.log("images", response);
              this.getImagesData = response;
            });
        } else {
          /** page not found, 404 */
        }
      });
  }

  getCommentsFromDatabase(pageID) {
    this.getComments.getComments(pageID).subscribe(response => {
      this.listAllComments = response;

      /** get ratings inside of comments  */
      this.getComments.getCommentsRatings(this.listAllComments);
    });
  }
}
