import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileContactInformationComponent } from './profile-contact-information.component';

describe('ProfileContactInformationComponent', () => {
  let component: ProfileContactInformationComponent;
  let fixture: ComponentFixture<ProfileContactInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileContactInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileContactInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
