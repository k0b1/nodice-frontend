import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-profile-carousel",
  templateUrl: "./profile-carousel.component.html",
  styleUrls: ["./profile-carousel.component.scss"]
})
export class ProfileCarouselComponent implements OnInit {
  @Input() getImagesData: any[];
  constructor() {}

  ngOnInit() {}

  slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrow: false,
    fade: true
    // "nextArrow":"<div class='nav-btn next-slide'></div>",
    // "prevArrow":"<div class='nav-btn prev-slide'></div>",
    // "dots":false,
    // "infinite": false
    // "asNavFor": ".carousel"
  };

  slideConfigSecond = {
    slidesToShow: 3,
    slidesToScroll: 1,
    // "asNavFor": ".carousel-second",
    nextArrow: "<div class='nav-btn next-slide'></div>",
    prevArrow: "<div class='nav-btn prev-slide'></div>",
    dots: true,
    arrows: true,
    centerMode: true,
    focusOnSelect: true,
    infinite: true
  };
}
