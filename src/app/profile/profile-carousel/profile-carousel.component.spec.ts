import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileCarouselComponent } from './profile-carousel.component';

describe('ProfileCarouselComponent', () => {
  let component: ProfileCarouselComponent;
  let fixture: ComponentFixture<ProfileCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
