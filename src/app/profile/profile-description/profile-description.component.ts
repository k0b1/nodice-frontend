import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-profile-description",
  templateUrl: "./profile-description.component.html",
  styleUrls: ["./profile-description.component.scss"]
})
export class ProfileDescriptionComponent implements OnInit {
  @Input() pageData: any[];
  @Input() pageID: number;
  @Input() contactData: {};

  constructor() {}

  ngOnInit() {}
}
