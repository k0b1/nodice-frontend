import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileGiveRatingComponent } from './profile-give-rating.component';

describe('ProfileGiveRatingComponent', () => {
  let component: ProfileGiveRatingComponent;
  let fixture: ComponentFixture<ProfileGiveRatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileGiveRatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileGiveRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
