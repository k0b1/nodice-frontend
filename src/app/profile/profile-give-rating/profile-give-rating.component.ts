import { Component, OnInit, Input, ViewChild, ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { PostCommentService } from "../../services/write-to-database/post-comment.service";

@Component({
  selector: "app-profile-give-rating",
  templateUrl: "./profile-give-rating.component.html",
  styleUrls: ["./profile-give-rating.component.scss"]
})
export class ProfileGiveRatingComponent implements OnInit {
  customerIsLoggedIn: boolean = false;
  displayTextBox: string = "none";
  customerWrote: string;

  @ViewChild("stars", { static: false }) stars: ElementRef;
  @ViewChild("rating", { static: false }) rating: ElementRef;

  @Input() isCustomerLoggedIn: boolean;
  @Input() customerID: number;
  @Input() customerName: string;
  @Input() pageID: number;
  @Input() listAllComments: any[];

  constructor(private postReview: PostCommentService) {}

  ngOnInit() {}

  onWriteReview() {
    this.displayTextBox = this.displayTextBox === "none" ? "block" : "none";
  }

  onGiveStars(event) {
    let starsArray = [...this.stars.nativeElement.children];

    for (let star in starsArray) {
      if (starsArray[star].attributes[3].value <= event.path[0].dataset.value) {
        starsArray[star].src =
          "wp-content/themes/nodice/dist/assets/images/stars/full-star.svg";
        this.rating.nativeElement.value = event.path[0].dataset.value;
      } else {
        starsArray[star].src =
          "wp-content/themes/nodice/dist/assets/images/stars/empty-star.svg";
      }
    }
  }

  onSubmitReview(review: NgForm) {
    /** important: is user logged in and is user a customer */

    if (review.value["write-review"]) {
      this.customerWrote = review.value["write-review"];
    }
    let userData = {
      author: this.customerID,
      post: this.pageID,
      author_name: this.customerName,
      content: this.customerWrote
    };
    
    let commentData;
    let commentID;
    let customerID;
    if (this.isCustomerLoggedIn) {
      this.postReview.postComment(userData).subscribe(response => {
        commentData = response;

        /** three lines below - display new comment and ratings immediately after publishing */
        this.listAllComments.unshift(response);
        let numberOfStars = Number(this.rating.nativeElement.value);
        this.listAllComments[0]["rating"] = [...Array(numberOfStars)].map(
          x => 0
        );

        /** define comments data, to be used for ratings */
        commentID = commentData.id;
        customerID = commentData.author;

        if (this.rating.nativeElement.value == 0) {
          this.rating.nativeElement.value = 1;
        }

        this.postReview
          .rateProfile(customerID, commentID, this.rating.nativeElement.value)
          .subscribe(response => {
          });
        this.displayTextBox = "none";
      });
    }
  }
}
