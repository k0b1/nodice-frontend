import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AuthenticateService } from "../../services/authenticate/authenticate.service";
import { Router } from "@angular/router";
import { environment } from "./../../../environments/environment";

//import { HeaderHomeComponent } from "../../../header/header.component";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  userID: string;
  userLocalStorage: string;

  errorMessage: string;
  isEmailValidated: boolean = false;
  isPasswordValidated: boolean = false;

    baseUrl: string = environment.urlBase;
    localhost: string = environment.localhost
  // userLogLocalStorage: string;
  constructor(
    private loginService: AuthenticateService,
    private router: Router
  ) {}

  ngOnInit() {}

  loginUser(userData: NgForm) {
    /** validate users */
    if (userData.value["email-address"] == "") {
      this.errorMessage = "Field can not be empty.";
      this.isEmailValidated = false;
    } else {
      this.isEmailValidated = true;
    }
    if (userData.value["password"] == "") {
      this.errorMessage = "Field can not be empty.";
      this.isPasswordValidated = false;
    } else {
      this.isPasswordValidated = true;
    }
    if (!this.isEmailValidated || !this.isPasswordValidated) {
      return;
    }
    return this.loginService.loginUsers(userData.value).subscribe(response => {
      if (response) {
        if (response.error) {
          this.errorMessage = response.error;
          this.isEmailValidated = false;
          return;
        } else {
          this.isEmailValidated = true;
        }
        this.loginService.authenticateUserObject.authenticateNonce =
          response.nonce;
        this.loginService.authenticateUserObject.authenticateUser = "true";
        this.loginService.authenticateUserObject.userID = response.user.ID;
        this.loginService.authenticateUserObject.displayName =
              response.user.data.display_name;
        this.userID = response.user.ID;
        /** set local storage nonce and log info to save state */
        localStorage.setItem(
          "user",
          this.loginService.authenticateUserObject.authenticateNonce
        );
        localStorage.setItem("isUserLoggedIn", "true");
        localStorage.setItem("userID", response.user.ID);
        localStorage.setItem("displayName", response.user.data.display_name);

        if (response.user.roles[0] === "administrator") {
          // window.location.href="https://nodice.gnf.dk/wp-admin/";
          window.location.href = this.localhost + "wp-admin/";
        } else if (response.user.roles[0] === "customer") {
          this.router.navigate([
            "/nodice-dashboard/" + this.userID + "/general"
          ]);
        } else if (response.user.roles[0] === "client") {
          this.router.navigate(["/nodice-dashboard/" + this.userID + "/general"]);
        }
      }
    });
  }
}
