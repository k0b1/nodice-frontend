import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { HeaderHomeComponent } from "../../shared/layout/header/header.component";
import { FormControl } from "@angular/forms";

// import { PostRestApiService } from '../services/write-to-database/post-rest-api.service';
import { AuthenticateService } from "../../services/authenticate/authenticate.service";
import { Router } from "@angular/router";
import { GetCategoriesService } from "../../services/get-from-database/get-categories.service";
@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.scss"]
})
export class SignupComponent implements OnInit {
  /** control tab displaying */
  showBusiness: string = "block";
  showOffer: string = "none";

  /** control multiple select dropdown displaying */
  showSelect: string = "block";
  countSelectDisplaying: number = 0;

  /** validate error messages */
  /** for client */
  validateUsername: boolean = false;
  validateBusinessPageName: boolean = false;
  validateEmail: boolean = false;
  validatePassword: boolean = false;
  validateCategories: boolean = false;
  validateBackend: boolean = false;

  /** for customer */
  validateFirstname = false;
  validateLastname = false;
  validateClientEmail = false;
  validateClientPassword = false;

  /** active tab color */
  activeTabClient: string = "active-tab";
  activeTabCustomer: string = "";

  /** display Categories in Select box */
  displayCategories: any[];

  /** validation */
  errorMessage: string = "Field must not be empty.";
  errorMessageEmail: string =
    "Field must not be empty and you need to enter proper email address";
  errorMessageBackend: string;

  constructor(
    private registerService: AuthenticateService,
    private router: Router,
    private getCategories: GetCategoriesService
  ) {}

  ngOnInit() {
    this.getCategoriesFromDatabase();
  }

  /** display or hide tabs in Sign up form */
  displayTab(selectedTab: string) {
    if (selectedTab == "business") {
      this.showBusiness = "block";
      this.showOffer = "none";
      this.activeTabClient = "active-tab";
      this.activeTabCustomer = "";
    } else if (selectedTab == "offers") {
      this.showBusiness = "none";
      this.showOffer = "block";
      this.activeTabClient = "";
      this.activeTabCustomer = "active-tab";
    }
  }

  registerClients(userData: NgForm) {
    return this.registerService
      .registerClients(userData.value)
      .subscribe(response => {
        /** check if there is an error */

        if (response.error) {
          this.validateBackend = true;
          this.errorMessageBackend = response.error;

          setTimeout(() => {
            this.validateBackend = false;
            this.errorMessageBackend = "";
          }, 3000);
        }
        if (response && response.status) {
          /** User is checked and approved. */

          /** First login newly created user... */
          this.registerService
            .loginUsers({
              "email-address": userData.value["email-address"],
              password: userData.value["business-password"]
            })
            .subscribe(response => {
              if (response.nonce == "error") {
                console.log(
                  "there was an error, while logging newly created user."
                );
              } else {
                console.log(
                  "Novoregistrovani korisnik je uspesno logovan: ",
                  response
                );

                this.registerService.authenticateUserObject.authenticateNonce =
                  response.nonce;
                this.registerService.authenticateUserObject.authenticateUser =
                  "true";
                this.registerService.authenticateUserObject.userID =
                  response.user.ID;
                this.registerService.authenticateUserObject.displayName =
                  response.user.data.display_name;
                //                this.userID = response.user.ID;
                /** set local storage nonce and log info to save state */
                localStorage.setItem(
                  "user",
                  this.registerService.authenticateUserObject.authenticateNonce
                );
                localStorage.setItem("isUserLoggedIn", "true");
                localStorage.setItem("userID", response.user.ID);
                localStorage.setItem(
                  "displayName",
                  response.user.data.display_name
                );

                /** ...then create profile page for newly registered user... */

                /** and redirect dear client/customer to thank-you page. */
                if (response.user.roles[0] == "client") {
                  this.registerService
                    .createPageForNewClient(
                      this.registerService.authenticateUserObject
                        .authenticateNonce,
                      userData.value["business_page_name"],
                      userData.value["select-categories"]
                    )
                    .subscribe(response => {
                      if (response === "Page Already Exist!!!!!!") {
                        return;
                      }
                    });
                  this.router.navigate(["/thank-you-client"]);
                }
              }
            });
        } else {
          /** there is a problem, give error message here */
        }
      });
  }

  registerCustomers(userData: NgForm) {
    if (userData.value["first-name"] == "") {
      this.validateFirstname = true;
      return;
    }
    if (userData.value["last-name"] == "") {
      this.validateLastname = true;
      return;
    }
    if (userData.value["offers-email-address"] == "") {
      this.validateClientEmail = true;
      return;
    }
    if (userData.value["offers-password"] == "") {
      this.validateClientPassword = true;
    }
    return this.registerService
      .registerCustomers(userData.value)
      .subscribe(response => {
        if (response.status) {
          if ((response.user.roles[0] = "customer")) {
            this.router.navigate(["/thank-you-customer"]);
          }
        } else {
          //and make error warning on the page
        }
      });
  }

  getCategoriesFromDatabase() {
    this.getCategories.getCategoriesFromDatabase().subscribe(response => {
      this.displayCategories = response;
    });
  }

  // OnMultiSelect(event) {
  //   this.countSelectDisplaying++;
  //   let addMe = document.querySelector('.cdk-overlay-container');
  //   let test2= document.querySelector('#test');
  //   if ( this.countSelectDisplaying % 2 == 1 ) {
  //     console.log( this.countSelectDisplaying )
  //     // let addMe = document.querySelector('.cdk-overlay-container');
  //     document.querySelector('#test').appendChild(addMe)
  //     // let test2= document.querySelector('#test');
  //     console.log( test2 );
  //   } else {
  //     console.log( this.countSelectDisplaying )
  //     // let addMe = document.querySelector('.cdk-overlay-container');
  //     // addMe.nativeElement.style.display = "none";
  //     document.querySelector('#test').removeChild(addMe)
  //     console.log( test2 );
  //   }

  // }
}
