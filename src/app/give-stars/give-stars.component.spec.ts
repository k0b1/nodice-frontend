import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiveStarsComponent } from './give-stars.component';

describe('GiveStarsComponent', () => {
  let component: GiveStarsComponent;
  let fixture: ComponentFixture<GiveStarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiveStarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiveStarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
