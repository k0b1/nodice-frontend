import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomepageComponent } from "./homepage/homepage.component";
import { ProfileComponent } from "./profile/profile.component";
import { SignupComponent } from "./auth/signup/signup.component";
import { ThankYouComponent } from "./thank-you/thank-you.component";
import { LoginComponent } from "./auth/login/login.component";
import { ThankYouCustomerComponent } from "./thank-you/thank-you-customer/thank-you-customer.component";
import { ThankYouClientComponent } from "./thank-you/thank-you-client/thank-you-client.component";
import { DashboardGeneralClientComponent } from "./dashboard/client/dashboard-general-client/dashboard-general-client.component";
import { DashboardGalleryComponent } from "./dashboard/client/dashboard-gallery/dashboard-gallery.component";
import { DashboardPlanComponent } from "./dashboard/client/dashboard-plan/dashboard-plan.component";
import { DashboardBillingComponent } from "./dashboard/client/dashboard-billing/dashboard-billing.component";
import { DashboardGeneralComponent } from "./dashboard/customer/dashboard-general/dashboard-general.component";
import { DashboardReviewsComponent } from "./dashboard/customer/dashboard-reviews/dashboard-reviews.component";
import { CategoriesComponent } from "./categories/categories.component";
import { SearchPageComponent } from "./search-page/search-page.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { AuthGuard } from "./auth-guard.service";

const routes: Routes = [
  {
    path: "",
    component: HomepageComponent,
    pathMatch: "full"
  },
  {
    path: "profile/:business-page-name",
    component: ProfileComponent,
    pathMatch: "full"
  },
  {
    path: "signup",
    component: SignupComponent,
    pathMatch: "full"
  },
  {
    path: "login",
    component: LoginComponent,
    pathMatch: "full"
  },
  {
    path: "thank-you",
    component: ThankYouComponent,
    pathMatch: "full"
  },
  {
    path: "thank-you-customer",
    component: ThankYouCustomerComponent,
    pathMatch: "full"
  },
  {
    path: "thank-you-client",
    component: ThankYouClientComponent,
    pathMatch: "full"
  },
  {
    path: "nodice-dashboard",
    canActivate: [AuthGuard],
    component: DashboardComponent,
    children: [
      {
        path: ":customerID/general",
        component: DashboardGeneralComponent
      },
      {
        path: ":customerID/reviews",
        component: DashboardReviewsComponent
      },
      {
        path: ":clientID/general",
        component: DashboardGeneralClientComponent
      },
      {
        path: ":clientID/gallery",
        component: DashboardGalleryComponent
      },
      {
        path: ":clientID/plan",
        component: DashboardPlanComponent
      },
      {
        path: ":clientID/billing",
        component: DashboardBillingComponent
      }
    ]
  },
  {
    path: "dashboard/:clientID/general",
    component: DashboardGeneralClientComponent,
    pathMatch: "full"
  },
  {
    path: "dashboard/:clientID/gallery",
    component: DashboardGalleryComponent,
    pathMatch: "full"
  },
  {
    path: "dashboard/:clientID/plan",
    component: DashboardPlanComponent,
    pathMatch: "full"
  },
  {
    path: "dashboard/:clientID/billing",
    component: DashboardBillingComponent,
    pathMatch: "full"
  },
  {
    path: "dashboard/customer/:customerID/general",
    component: DashboardGeneralComponent,
    pathMatch: "full"
  },
  {
    path: "dashboard/customer/:customerID/reviews",
    component: DashboardReviewsComponent,
    pathMatch: "full"
  },
  {
    path: "categories",
    component: CategoriesComponent,
    pathMatch: "full"
  },
  {
    path: "search",
    component: SearchPageComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
